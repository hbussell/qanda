Awesome Question and Answer Tool.

## Installation

Get the package dependencies with `npm install`

## Local Server

Start a local server with `npm start`

The page will then be accessible on either url.
http://localhost:3000/
http://192.168.3.209:3000/

## Deploy to production

Create a production build of the application with `npm run build`

Then you can deploy to Firebase hosting
 - `firebase login`
 - `firebase deploy`

 