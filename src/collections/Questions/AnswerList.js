import React, {Component} from 'react';
import {RaisedButton} from 'material-ui';
import {connect} from 'react-redux';

const QuestionItem = props => {
  const {question, onRemove} = props;
  return (
    <div className="question-item">
      <div className="question">
        {question.question}
      </div>
      <div className="answer">
        {question.answer}
      </div>
      <div>
        <RaisedButton 
          label="Remove"
          onClick={onRemove}
        />
      </div>
    </div>
  )
}

class AnswerList extends Component {

  constructor(...args) {
    super(...args);
    this.state = {
      sorting: false
    };
  }

  onSort = () => {
    this.setState({
      sorting: !this.state.sorting
    });
  }

  onClear = () => {
    this.props.clearQuestions();
  }

  render() {
    const {questions, removeQuestion} = this.props;
    const {sorting} = this.state;

    if (sorting) {
      questions.sort(function(a ,b) {return a.question.localeCompare(b.question);});
    }
    
    return (
      <div className="answers">
        <h2>Created questions</h2>
        
        {questions.map(function(question, i){
          return <QuestionItem question={question} key={i} onRemove={()=>{ removeQuestion(question) }}/>;
        })}

      <div>
        <RaisedButton
          label="Sort"
          onClick={this.onSort}
          primary={!sorting}
          />
        <RaisedButton
          label="Clear questions"
          onClick={this.onClear}
          secondary={true}
        />
      </div>  
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    questions : state.questions.questions
  }
}

const mapDispatchToProps = dispatch => {
  return {
    removeQuestion : (question) => dispatch({
      type : 'REMOVE_QUESTION',
      question: question
    }),
    clearQuestions: () => dispatch({
      type : 'CLEAR_QUESTIONS',
    })
  }
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AnswerList)