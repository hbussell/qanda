import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { TextField } from 'material-ui';
import { RaisedButton } from 'material-ui';

/**
 * Create MaterialUI TextField
 * @param {*} param0 
 */
const renderTextField = ({
  input,
  label,
  meta: { touched, error },
  ...custom
}) => (
  <TextField
    hintText={label}
    floatingLabelText={label}
    errorText={touched && error}
    {...input}
    {...custom}
  />
)

/**
 * Validate form fields.
 * @param {array} values 
 */
const validate = function(values) {
  const errors = {};
  const requiredFields = [
    'question',
    'answer',
  ];
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required';
    }
  });
  return errors;
}

/**
 * Question Form.
 * @param {*} props 
 */
const QuestionForm = props => {
  const { handleSubmit, pristine, submitting } = props
  return (
    <form onSubmit={handleSubmit}>
      <h2>Create a new question</h2>
      <div>
        <Field
          name="question"
          component={renderTextField}
          label="Question"
        />
      </div>
      <div>
        <Field
          name="answer"
          component={renderTextField}
          label="Answer"
        />
      </div>
      <div>
        <RaisedButton 
          primary={true} 
          type="submit" 
          label="Submit"
          disabled={pristine || submitting}/>
      </div>
    </form>
  )
}

export default reduxForm({
  form: 'QuestionForm', // a unique identifier for this form
  validate
})(QuestionForm)